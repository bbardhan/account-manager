package com.acmebank.accountmanager.exception;

public class AccountException extends RuntimeException {
	private static final long serialVersionUID = 121220609920887471L;

	public AccountException(String errorMessage) {
		super(errorMessage);
	}
}
