package com.acmebank.accountmanager.model;

import java.math.BigDecimal;

public class TransferAmountRequest {
	private final String fromAccountNumber;
	private final String toAccountNumber;
	private final BigDecimal amount;

	public TransferAmountRequest(String fromAccountNumber, String toAccountNumber, BigDecimal amount) {
		this.fromAccountNumber = fromAccountNumber;
		this.toAccountNumber = toAccountNumber;
		this.amount = amount;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}
}
