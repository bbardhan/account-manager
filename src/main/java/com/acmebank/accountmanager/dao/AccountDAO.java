package com.acmebank.accountmanager.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.acmebank.accountmanager.model.Account;

public interface AccountDAO extends JpaRepository<Account, Integer> {
	Optional<Account> findByAccountNumber(String accountNumber);
}
