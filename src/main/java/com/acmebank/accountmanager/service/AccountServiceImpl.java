package com.acmebank.accountmanager.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.acmebank.accountmanager.dao.AccountDAO;
import com.acmebank.accountmanager.exception.AccountException;
import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.TransferAmountRequest;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class AccountServiceImpl implements AccountService {

	private AccountDAO accountDAO;

	@Autowired
	public AccountServiceImpl(AccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}

	@Override
	public List<Account> getAllAccounts() {
		return accountDAO.findAll();
	}

	@Override
	public Optional<Account> getAccountByAccountNumber(String accountNumber) {
		checkForEmpty(accountNumber);
		return accountDAO.findByAccountNumber(accountNumber);
	}

	@Override
	public List<Account> transferBalanceRequest(TransferAmountRequest request) {
		validateTransferAmount(request);
		checkForEmpty(request.getFromAccountNumber());
		checkForEmpty(request.getToAccountNumber());

		Optional<Account> fromAccount = accountDAO.findByAccountNumber(request.getFromAccountNumber());
		validateAccount(fromAccount, request.getFromAccountNumber());

		Optional<Account> toAccount = accountDAO.findByAccountNumber(request.getToAccountNumber());
		validateAccount(toAccount, request.getToAccountNumber());

		validateSufficientBalance(request, fromAccount);

		Account fromAcnt = fromAccount.get();
		Account toAcnt = toAccount.get();

		fromAcnt.setBalance(fromAcnt.getBalance().subtract(request.getAmount()));
		toAcnt.setBalance(toAcnt.getBalance().add(request.getAmount()));

		List<Account> list = new ArrayList<>();

		list.add(fromAcnt);
		list.add(toAcnt);
		
		return list;
	}

	private void checkForEmpty(String accountNumber) {
		if (StringUtils.isEmpty(accountNumber)) {
			throw new AccountException("account number can not be blank");
		}
	}

	private void validateAccount(Optional<Account> fromAccount, String accountNumber) {
		if (!fromAccount.isPresent()) {
			throw new AccountException(accountNumber + " : this account number doesn't exit");
		}
	}

	private void validateTransferAmount(TransferAmountRequest request) {
		if (request.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new AccountException("Transfer Amount should be greater than zero");
		}
	}

	private void validateSufficientBalance(TransferAmountRequest request, Optional<Account> fromAccount) {
		if (request.getAmount().compareTo(fromAccount.get().getBalance()) > 0) {
			throw new AccountException("Insufficient balance in source account: " + request.getFromAccountNumber());
		}
	}
}
