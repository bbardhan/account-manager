package com.acmebank.accountmanager.service;

import java.util.List;
import java.util.Optional;

import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.TransferAmountRequest;

public interface AccountService {
	List<Account> getAllAccounts();
	Optional<Account> getAccountByAccountNumber(String accountNumber);
	List<Account> transferBalanceRequest(TransferAmountRequest request);
}
