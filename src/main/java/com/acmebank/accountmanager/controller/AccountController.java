package com.acmebank.accountmanager.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.acmebank.accountmanager.exception.AccountException;
import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.TransferAmountRequest;
import com.acmebank.accountmanager.service.AccountService;

@RestController
@RequestMapping(path = "/api/v1/account")
public class AccountController {
	private AccountService service;

	@Autowired
	public AccountController(AccountService service) {
		this.service = service;
	}

	@GetMapping(path = "/all")
	public List<Account> getAllAccounts() {
		return service.getAllAccounts();

	}

	@GetMapping(path = "/{accountNumber}")
	public Account getAccount(@PathVariable("accountNumber") String accountNumber) {

		return service.getAccountByAccountNumber(accountNumber)
				.orElseThrow(() -> new AccountException(accountNumber + " : account number doesn't exit"));
	}

	@GetMapping(path = "/{accountNumber}/balance")
	public String getAccountBalance(@PathVariable("accountNumber") String accountNumber) {
		Account account = service.getAccountByAccountNumber(accountNumber)
				.orElseThrow(() -> new AccountException(accountNumber + " : account number doesn't exit"));

		return account.getBalance().toString();
	}

	@PostMapping(path = "/transfer")
	public ResponseEntity<Object> transferAmount(@RequestBody TransferAmountRequest request) {
		service.transferBalanceRequest(request);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{all}")
				.buildAndExpand("/api/v1/account/all").toUri();
		return ResponseEntity.created(location).build();
	}

}
