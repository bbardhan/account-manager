package com.acmebank.accountmanager;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.acmebank.accountmanager.dao")
@EntityScan("com.acmebank.accountmanager.model")
public class H2JpaConfig {
}
