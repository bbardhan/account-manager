package com.acmebank.accountmanager;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import com.acmebank.accountmanager.dao.AccountDAO;
import com.acmebank.accountmanager.exception.AccountException;
import com.acmebank.accountmanager.model.Account;
import com.acmebank.accountmanager.model.TransferAmountRequest;
import com.acmebank.accountmanager.service.AccountService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { AccountManagerApplication.class,
		H2JpaConfig.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountManagerApplicationTests {

	@Autowired
	private AccountService service;

	@Autowired
	private AccountDAO dao;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testAccountRestAPI() throws RestClientException, URISyntaxException {

		final BigDecimal balance = new BigDecimal("500000.00");
		final String accountNumber = "5555555555";
		final Account account = new Account(1, accountNumber, balance, "HKD");
		final Account accountFromDB = dao.save(account);

		final String url = createURLWithPort("/5555555555");

		Account response = restTemplate.getForObject(new URI(url), Account.class);

		Assert.assertTrue(accountFromDB.equals(response));

		dao.delete(accountFromDB);
	}

	@Test
	public void testBalanceRestAPI() throws RestClientException, URISyntaxException {
		final BigDecimal balance = new BigDecimal("500000.00");
		final String accountNumber = "5555555555";
		final Account account = new Account(1, accountNumber, balance, "HKD");
		final Account accountFromDB = dao.save(account);

		String url = createURLWithPort("/5555555555/balance");

		String response = restTemplate.getForObject(new URI(url), String.class);

		Assert.assertEquals(balance.toString(), response);

		dao.delete(accountFromDB);
	}

	@Test
	public void testAccountTransferAmountRestAPI() throws RestClientException, URISyntaxException {

		final BigDecimal balance = new BigDecimal("500000.00");
		final String fromAccountNumber = "5555555555";
		final Account fromAccount = new Account(1, fromAccountNumber, balance, "HKD");
		final Account fromAccountFromDB = dao.save(fromAccount);

		final String toAccountNumber = "6666666666";
		final Account toAccount = new Account(2, toAccountNumber, balance, "HKD");
		final Account toAccountFromDB = dao.save(toAccount);

		final BigDecimal transferAmount = new BigDecimal("100000.00");

		TransferAmountRequest request = new TransferAmountRequest(fromAccountNumber, toAccountNumber, transferAmount);

		String url = createURLWithPort("/transfer");

		ResponseEntity<Object> response = restTemplate.postForEntity(url, request, Object.class);

		Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());

		Assert.assertEquals(new BigDecimal("400000.00"),
				service.getAccountByAccountNumber(fromAccountNumber).get().getBalance());
		Assert.assertEquals(new BigDecimal("600000.00"),
				service.getAccountByAccountNumber(toAccountNumber).get().getBalance());

		dao.delete(fromAccountFromDB);
		dao.delete(toAccountFromDB);
	}

	@Test
	public void tesInvalidAccount() {
		Assert.assertFalse(service.getAccountByAccountNumber("123987456").isPresent());
	}

	@Test
	public void testBlankAccount() {
		try {
			service.getAccountByAccountNumber("");
		} catch (Exception e) {
			Assert.assertTrue(e.getClass() == AccountException.class);
			Assert.assertTrue(e.getMessage().equals("account number can not be blank"));
		}
	}

	@Test
	public void testAccountTransferAmountForMultipleRequest() throws InterruptedException {
		final BigDecimal balance = new BigDecimal("500000.00");
		final String fromAccountNumber = "5555555555";
		final Account fromAccount = new Account(1, fromAccountNumber, balance, "HKD");
		final Account fromAccountFromDB = dao.save(fromAccount);

		final String toAccountNumber = "6666666666";
		final Account toAccount = new Account(2, toAccountNumber, balance, "HKD");
		final Account toAccountFromDB = dao.save(toAccount);

		final BigDecimal transferAmount = new BigDecimal("4000.00");
		final TransferAmountRequest request = new TransferAmountRequest(fromAccountNumber, toAccountNumber,
				transferAmount);

		IntStream.rangeClosed(1, 10).forEach((i) -> {
			service.transferBalanceRequest(request);
		});

		Assert.assertEquals(new BigDecimal("460000.00"),
				service.getAccountByAccountNumber(fromAccountNumber).get().getBalance());
		Assert.assertEquals(new BigDecimal("540000.00"),
				service.getAccountByAccountNumber(toAccountNumber).get().getBalance());

		dao.delete(fromAccountFromDB);
		dao.delete(toAccountFromDB);

	}

	@Test
	public void testAccountTransferAmountExceedsBalance() {
		final BigDecimal balance = new BigDecimal("500000.00");
		final String fromAccountNumber = "5555555555";
		final Account fromAccount = new Account(1, fromAccountNumber, balance, "HKD");
		final Account fromAccountFromDB = dao.save(fromAccount);

		final String toAccountNumber = "6666666666";
		final Account toAccount = new Account(2, toAccountNumber, balance, "HKD");
		final Account toAccountFromDB = dao.save(toAccount);

		final BigDecimal transferAmount = new BigDecimal("900000.00");

		TransferAmountRequest request = new TransferAmountRequest(fromAccountNumber, toAccountNumber, transferAmount);

		try {
			service.transferBalanceRequest(request);
		} catch (Exception e) {
			Assert.assertTrue(e.getClass() == AccountException.class);
			Assert.assertTrue(e.getMessage().contains("Insufficient balance in source account"));
		}

		dao.delete(fromAccountFromDB);
		dao.delete(toAccountFromDB);
	}

	@Test
	public void testAccountTransferNegativeAmount() {
		final BigDecimal transferAmount = new BigDecimal("-100.00");

		TransferAmountRequest request = new TransferAmountRequest("11111111", "22222222", transferAmount);

		try {
			service.transferBalanceRequest(request);
		} catch (Exception e) {
			Assert.assertTrue(e.getClass() == AccountException.class);
			Assert.assertTrue(e.getMessage().equals("Transfer Amount should be greater than zero"));
		}
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + "/api/v1/account" + uri;
	}

}
