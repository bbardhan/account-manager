# Bank Account Manager API
RESTful API Spring Boot Banking Account Management application   

It's been created to meet the following requirements:
* You can get the balance of the account using REST API
* You would be able to transfer the balance between two accounts using REST API
* Swagger UI has been provided to display API on the browser to execute API operations easily